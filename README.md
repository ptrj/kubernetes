> https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

# Ubunutu
## Install Ubuntu (16.04/18.04) LTS minimal
## Update & Upgrade
```
apt-get update && apt-get upgrade -y
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
## Disable swap
Edit /etc/fstab
```
# /dev/mapper/ubuntu--vg-swap_1 none            swap    sw              0       0
$ swapoff /dev/mapper/ubuntu--vg-swap_1
```
```
swapoff -a
```
Setup hostname and static IP. Example: master.kube.lab 192.168.250.10
## Install Docker
> On each of your machines, install Docker. Version 17.03 is recommended, but 1.11, 1.12 and 1.13 are known to work as well. Versions 17.06+ might work, but have not yet been tested and verified by the Kubernetes node team. Keep track of the latest verified Docker version in the Kubernetes release notes.

### Install Docker from Ubuntu’s repositories
```
apt-get install -y docker.io
```

### Official Docker.com instruction (v18.x)
https://docs.docker.com/install/linux/docker-ce/ubuntu/
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get install docker-ce docker-ce-cli containerd.io
```
> [WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
```
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d
# Restart Docker
systemctl daemon-reload
systemctl restart docker
```

## Install Kubernetes
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
```
# CentOS 7
## Install CentOS 7 minimal
## Update & Upgrade
```
yum update -y
```
## Disable swap
Edit /etc/fstab
```
# /dev/mapper/centos-swap swap                    swap    defaults	  0 0
$ swapoff /dev/mapper/centos-swap
```
## Install Docker
> On each of your machines, install Docker. Version 17.03 is recommended, but 1.11, 1.12 and 1.13 are known to work as well. Versions 17.06+ might work, but have not yet been tested and verified by the Kubernetes node team. Keep track of the latest verified Docker version in the Kubernetes release notes.

### Docker from EPEL Repository (v1.13.x)
```
$ yum install epel-release
$ yum install docker
$ systemctl enable docker.service && systemctl start docker.service
```

### Official Docker.com instruction (v18.x)
> This version didn't work properly

https://docs.docker.com/install/linux/docker-ce/centos/
```
$ yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
$ yum install docker-ce
$ systemctl enable docker.service && ssystemctl start docker.service
```
## Install Kubernetes
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
setenforce 0
yum install -y kubelet kubeadm kubectl
systemctl enable kubelet && systemctl start kubelet
```
> Disabling SELinux by running **setenforce 0** is required to allow containers to access the host filesystem, which is required by pod networks for example. You have to do this until SELinux support is improved in the kubelet.
> Some users on RHEL/CentOS 7 have reported issues with traffic being routed incorrectly due to iptables being bypassed. You should ensure **net.bridge.bridge-nf-call-iptables** is set to 1 in your **sysctl** config, e.g.
>```
> cat <<EOF >  /etc/sysctl.d/k8s.conf
> net.bridge.bridge-nf-call-ip6tables = 1
> net.bridge.bridge-nf-call-iptables = 1
> EOF
> sysctl --system
>```

# Install Kubernetes Master

> KUBEADM IS CURRENTLY IN BETA
> 
> But please, try it out and give us feedback at:
> https://github.com/kubernetes/kubeadm/issues
> and at-mention @kubernetes/sig-cluster-lifecycle-bugs
> or @kubernetes/sig-cluster-lifecycle-feature-requests

```
kubeadm init --pod-network-cidr=10.244.0.0/16

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
> You should now deploy a pod network to the cluster.
> Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
>   https://kubernetes.io/docs/concepts/cluster-administration/addons/

## Install Flannel PodNetwork
> Set /proc/sys/net/bridge/bridge-nf-call-iptables to 1 by running sysctl net.bridge.bridge-nf-call-iptables=1 to pass bridged IPv4 
> traffic to iptables’ chains. This is a requirement for some CNI plugins to work, for more information please see here.
>   https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#pod-network
> Note that flannel works on amd64, arm, arm64, ppc64le and s390x under Linux. Windows (amd64) is claimed as supported in v0.11.0 but the usage is undocumented.
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml
```
> If we have a single node, then we must allow to scale a "non-kube-system" pods

```
kubectl describe node $(hostname -f) | grep -i taint
kubectl taint nodes $(hostname -f) node-role.kubernetes.io/master-
or
kubectl taint nodes $(hostname -f) node.kubernetes.io/not-ready-
kubectl describe node $(hostname -f) | grep -i taint
```
If doesn't works try "hostname" without "-f" just $(hostname)

Wait a few minutes and check if everything works
```
# kubectl get all --all-namespaces

NAMESPACE     NAME                                 READY   STATUS    RESTARTS   AGE
kube-system   pod/coredns-6955765f44-lzlz2         1/1     Running   0          3m35s
kube-system   pod/coredns-6955765f44-m8pmv         1/1     Running   0          3m35s
kube-system   pod/etcd-node01                      1/1     Running   0          3m46s
kube-system   pod/kube-apiserver-node01            1/1     Running   0          3m45s
kube-system   pod/kube-controller-manager-node01   1/1     Running   0          3m46s
kube-system   pod/kube-flannel-ds-amd64-7rlh5      1/1     Running   0          94s
kube-system   pod/kube-proxy-c5khn                 1/1     Running   0          3m35s
kube-system   pod/kube-scheduler-node01            1/1     Running   0          3m46s

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  3m51s
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   3m49s

NAMESPACE     NAME                                     DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                 AGE
kube-system   daemonset.apps/kube-flannel-ds-amd64     1         1         1       1            1           <none>                        94s
kube-system   daemonset.apps/kube-flannel-ds-arm       0         0         0       0            0           <none>                        94s
kube-system   daemonset.apps/kube-flannel-ds-arm64     0         0         0       0            0           <none>                        94s
kube-system   daemonset.apps/kube-flannel-ds-ppc64le   0         0         0       0            0           <none>                        94s
kube-system   daemonset.apps/kube-flannel-ds-s390x     0         0         0       0            0           <none>                        94s
kube-system   daemonset.apps/kube-proxy                1         1         1       1            1           beta.kubernetes.io/os=linux   3m49s

NAMESPACE     NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/coredns   2/2     2            2           3m49s

NAMESPACE     NAME                                 DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/coredns-6955765f44   2         2         2       3m35s
```
# Upgrade Kubernetes
> https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/
```
# apt-get upgrade kubeadm kubectl kubelet
# kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[preflight] Running pre-flight checks.
[upgrade] Making sure the cluster is healthy:
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.17.0
[upgrade/versions] kubeadm version: v1.17.1
[upgrade/versions] Latest stable version: v1.17.1
[upgrade/versions] Latest version in the v1.17 series: v1.17.1

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       AVAILABLE
Kubelet     1 x v1.17.0   v1.17.1

Upgrade to the latest version in the v1.17 series:

COMPONENT            CURRENT   AVAILABLE
API Server           v1.17.0   v1.17.1
Controller Manager   v1.17.0   v1.17.1
Scheduler            v1.17.0   v1.17.1
Kube Proxy           v1.17.0   v1.17.1
CoreDNS              1.6.5     1.6.5
Etcd                 3.4.3     3.4.3-0

You can now apply the upgrade by executing the following command:

	kubeadm upgrade apply v1.17.1

_____________________________________________________________________

# kubeadm upgrade apply v1.17.1
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[preflight] Running pre-flight checks.
[upgrade] Making sure the cluster is healthy:
[upgrade/version] You have chosen to change the cluster version to "v1.17.1"
[upgrade/versions] Cluster version: v1.17.0
[upgrade/versions] kubeadm version: v1.17.1
[upgrade/confirm] Are you sure you want to proceed with the upgrade? [y/N]: y
[upgrade/prepull] Will prepull images for components [kube-apiserver kube-controller-manager kube-scheduler etcd]
[upgrade/prepull] Prepulling image for component etcd.
[upgrade/prepull] Prepulling image for component kube-controller-manager.
[upgrade/prepull] Prepulling image for component kube-apiserver.
[upgrade/prepull] Prepulling image for component kube-scheduler.
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-etcd
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-controller-manager
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-apiserver
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-etcd
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[upgrade/prepull] Prepulled image for component etcd.
[upgrade/prepull] Prepulled image for component kube-apiserver.
[upgrade/prepull] Prepulled image for component kube-scheduler.
[upgrade/prepull] Prepulled image for component kube-controller-manager.
[upgrade/prepull] Successfully prepulled the images for all the control plane components
[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.17.1"...
Static pod: kube-apiserver-node01 hash: 3a80ce8bc698ab7f00e920525eb00a17
Static pod: kube-controller-manager-node01 hash: 14c1f57384787ba5a042541648f3fbeb
Static pod: kube-scheduler-node01 hash: ff67867321338ffd885039e188f6b424
[upgrade/etcd] Upgrading to TLS for etcd
[upgrade/etcd] Non fatal issue encountered during upgrade: the desired etcd version for this Kubernetes version "v1.17.1" is "3.4.3-0", but the current etcd version is "3.4.3". Won't downgrade etcd, instead just continue
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests136725487"
W0121 13:48:42.227184   13413 manifests.go:214] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-01-21-13-48-41/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-apiserver-node01 hash: 3a80ce8bc698ab7f00e920525eb00a17
Static pod: kube-apiserver-node01 hash: 3a80ce8bc698ab7f00e920525eb00a17
Static pod: kube-apiserver-node01 hash: 77772b81719c62f8dad114ae61da6aed
[apiclient] Found 1 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-01-21-13-48-41/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-controller-manager-node01 hash: 14c1f57384787ba5a042541648f3fbeb
Static pod: kube-controller-manager-node01 hash: be521620979c103255480f04b313bb33
[apiclient] Found 1 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-01-21-13-48-41/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-scheduler-node01 hash: ff67867321338ffd885039e188f6b424
Static pod: kube-scheduler-node01 hash: 11d278345de05e1c5c61a63a8a1d78b2
[apiclient] Found 1 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.17" in namespace kube-system with the configuration for the kubelets in the cluster
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.17" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons]: Migrating CoreDNS Corefile
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.17.1". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.

# kubectl version
Client Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.1", GitCommit:"d224476cd0730baca2b6e357d144171ed74192d6", GitTreeState:"clean", BuildDate:"2020-01-14T21:04:32Z", GoVersion:"go1.13.5", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.1", GitCommit:"d224476cd0730baca2b6e357d144171ed74192d6", GitTreeState:"clean", BuildDate:"2020-01-14T20:56:50Z", GoVersion:"go1.13.5", Compiler:"gc", Platform:"linux/amd64"}
```

# Install Kubernetes Dashboard
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
```
## Create admin user
```
cat > /tmp/admin-user.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
EOF

kubectl create -f /tmp/admin-user.yaml

cat > /tmp/cluster-role-binding.yaml <<EOF
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF

kubectl create -f /tmp/cluster-role-binding.yaml
```
## Your Auth Token is
```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user-token | awk '{print $1}') | grep "token:"
```
## Test
### Start
```
# kubectl proxy
Starting to serve on 127.0.0.1:8001
```
### Port forward 8001 (macOS & Linux Desktop)
```
ssh -L 8001:localhost:8001 my_user@master-node
```
> Windows Putty port forward example: https://blog.devolutions.net/2017/4/how-to-configure-an-ssh-tunnel-on-putty

### Open URL and Login with your token
```
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login
```
## Open Dashboard on Master Node IP (recommended for development)
```
kubectl -n kubernetes-dashboard edit service kubernetes-dashboard
```
Change **type: ClusterIP** to **type: NodePort** and save.
Then find out the port
```
# kubectl -n kubernetes-dashboard get service kubernetes-dashboard
NAME                   TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)         AGE
kubernetes-dashboard   NodePort   10.108.130.213   <none>        443:32208/TCP   29m
```
Open URL and login with your token
```
https://192.168.250.10:32208/
```
## For Production enviroment read
> https://github.com/kubernetes/dashboard/wiki/Accessing-Dashboard---1.7.X-and-above  
> https://blog.heptio.com/on-securing-the-kubernetes-dashboard-16b09b1b7aca  
> https://kubernetes.io/docs/concepts/services-networking/ingress/  

# Reset
If you want to remove everything and try it again
```
# kubeadm reset
[preflight] Running pre-flight checks.
[reset] Stopping the kubelet service.
[reset] Unmounting mounted directories in "/var/lib/kubelet"
[reset] Removing kubernetes-managed containers.
[reset] Deleting contents of stateful directories: [/var/lib/kubelet /etc/cni/net.d /var/lib/dockershim /var/run/kubernetes /var/lib/etcd]
[reset] Deleting contents of config directories: [/etc/kubernetes/manifests /etc/kubernetes/pki]
[reset] Deleting files: [/etc/kubernetes/admin.conf /etc/kubernetes/kubelet.conf /etc/kubernetes/bootstrap-kubelet.conf /etc/kubernetes/controller-manager.conf /etc/kubernetes/scheduler.conf]

# rm -rf ~/.kube/
```
# Issues
## kubelet:  Failed to get system container stats for "/system.slice/docker.service"
Edit: nano /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
Add:
```
Environment="KUBELET_CGROUP_ARGS=--cgroup-driver=cgroupfs --runtime-cgroups=/systemd/system.slice --kubelet-cgroups=/systemd/system.slice"
```
Change:
```
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS
```
to
```
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CGROUP_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS
```
Restart kubelet
```
systemctl daemon-reload
systemctl restart kubelet.service
```
